package com.book.main.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.book.main.dto.BookDto;
import com.book.main.entity.Book;
import com.book.main.exception.BookNotFoundException;
import com.book.main.mapper.BookMapper;
import com.book.main.repository.BookRepository;
import com.book.main.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	private final BookRepository repository;
	private final BookMapper mapper;
	
	public BookServiceImpl(BookRepository repository, BookMapper mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}
	
	@Override
	public List<BookDto> getAllBooks() {
		List<Book> bookList = repository.findAll();
		List<BookDto> bookDtoList = bookList.stream().map(obj -> mapper.entityToDto(obj)).collect(Collectors.toList());
		return bookDtoList;
	}
	
	@Override
	public BookDto createBook(BookDto dto) {
		Book book = mapper.dtoToEntity(dto);
		book = saveBook(book);
		return mapper.entityToDto(book);
	}
	
	@Override
	public BookDto getBookById(int id) {
		Book book = repository.findById(Long.valueOf(id)).orElseThrow(()->new BookNotFoundException("Book not found"));
		return mapper.entityToDto(book);
	}
	
	@Override
	public void deleteBookById(int id) {
		repository.deleteById(Long.valueOf(id));
	}
	
	@Override
	public BookDto updateBook(BookDto dto) {
		Book book = repository.findById(Long.valueOf(dto.getBookId())).orElseThrow(()->new BookNotFoundException("Book not found"));
		book = mapper.updateEntityFromDTO(book, dto);
		book = saveBook(book);
		return mapper.entityToDto(book);
	}

	public Book saveBook(Book entity) {
		return repository.save(entity);
	}
}
