package com.book.main.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.book.main.dto.BookDto;
import com.book.main.entity.Book;

public interface BookService {

	public List<BookDto> getAllBooks();
	
	public BookDto getBookById(int id);
	
	public void deleteBookById(int id);
	
	public BookDto createBook(BookDto dto);
	
	public BookDto updateBook(BookDto dto);
}
