package com.book.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.book.main.dto.BookDto;
import com.book.main.service.BookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/books")
@RefreshScope
@Api(value="BookController", description="Operations pertaining to Books in Library project")
public class BookController {

	@Value("${owner.name}")
	String owner;

	private final BookService service;

	public BookController(BookService service) {
		this.service = service;
	}

	@ApiOperation(value = "View a list of books", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping
	public ResponseEntity<List<BookDto>> getAllBooks() {
		List<BookDto> bookDtoList = service.getAllBooks();
		return new ResponseEntity<>(bookDtoList, HttpStatus.OK);
	}

	@ApiOperation(value = "View a Book based on id", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/{book_id}")
	public ResponseEntity<BookDto> getBookById(@PathVariable int book_id) {
		BookDto bookDto = service.getBookById(book_id);
		return new ResponseEntity<>(bookDto, HttpStatus.OK);
	}

	@ApiOperation(value = "Create a Book", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping
	public ResponseEntity<BookDto> createBook(@RequestBody BookDto dto) {
		BookDto bookDto = service.createBook(dto);
		return new ResponseEntity<>(bookDto, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete a Book", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@DeleteMapping("/{book_id}")
	public ResponseEntity<String> deleteBookById(@PathVariable int book_id) {
		service.deleteBookById(book_id);
		return new ResponseEntity<>("Book Deleted Successfully", HttpStatus.OK);
	}

	@ApiOperation(value = "Update a Book", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PutMapping("/books")
	public ResponseEntity<BookDto> updateBook(@RequestBody BookDto dto) {
		BookDto bookDto = service.createBook(dto);
		return new ResponseEntity<>(bookDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/demo")
	public ResponseEntity<String> welcomeText() {
		return new ResponseEntity<>(owner, HttpStatus.OK);
	}
}
