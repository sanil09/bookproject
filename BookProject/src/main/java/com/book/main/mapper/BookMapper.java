package com.book.main.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import com.book.main.dto.BookDto;
import com.book.main.entity.Book;

@Mapper(componentModel = "spring")
public interface BookMapper {
	
	@Mappings({
	      @Mapping(target="bookId", source="entity.id")
	    })
	    BookDto entityToDto(Book entity);
	
	@Mappings({
	      @Mapping(target="id", source="dto.bookId")
	    })
	    Book dtoToEntity(BookDto dto);
	
	    @Mapping(target = "id", ignore = true)
	    public abstract Book updateEntityFromDTO(@MappingTarget Book entity, BookDto dto);
}
